package com.example.instagram_kotlinapp.Profile

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.instagram_kotlinapp.R
import com.example.instagram_kotlinapp.utils.BottomnavigationViewHelper
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.activity_profile_settings.*
import kotlinx.android.synthetic.main.activity_profile_settings.bottomNavigationView

class ProfileSettingsActivity : AppCompatActivity() {
    private val ACTIVITY_NO =
        4 //bottom_navigation_menu.xml deki temlarin index nosudur.Ona gore hangi item oldugumusu anlariz
    private val TAG = "Profile Activiy"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_settings)

        setupNavigationView() //navigation vermedik
        setupToolbar()
        fragmentNavigations()

    }

    private fun fragmentNavigations() {

        txtProfileEditAccount.setOnClickListener {

            profileSettingsRoot.visibility=View.GONE //Profile settings xml tamamen gürünümü kaldırıyoruz gizleme yapiyoruz
            var transaction=supportFragmentManager.beginTransaction() //fragment islemini baslatiyoruz
            transaction.replace(R.id.profileSettingsContainer,ProfileEditFragment()).addToBackStack("editProfileFragmenEklendi") //ProfileEditFragment yerine yerleştiriyoruz

            transaction.commit() // islem  bittiginde commitlemeyi unutma

        }


        txtSingOut.setOnClickListener {

            profileSettingsRoot.visibility=View.GONE //Profile settings xml tamamen gürünümü kaldırıyoruz gizleme yapiyoruz(INVISIBLE gizler ama yer kaplar)
            var transaction=supportFragmentManager.beginTransaction()
            transaction.replace(R.id.profileSettingsContainer,SignOutFragment())
            transaction.addToBackStack("signOutFragmentEklendi") //geri gittignde fragmentler gecis yapmasi icin kullaniyoruz(normalde  geri basildiginda activtylere gecis yapar)

            transaction.commit()
        }




    }

    private fun setupToolbar() {
        imgBack.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
              onBackPressed() //onceki activty geri don
            }
        })
    }

    override fun onBackPressed() {
        profileSettingsRoot.visibility=View.VISIBLE
        super.onBackPressed()
    }


    fun setupNavigationView() { //basta butonun yazisi olmasin basta kapattik
        BottomnavigationViewHelper.setupButtomNavigationView(bottomNavigationView)
        BottomnavigationViewHelper.setupNavigation(this, bottomNavigationView) //activty context gonderdik
        var menu = bottomNavigationView.menu //XML getirdik
        var menuItem = menu.getItem(ACTIVITY_NO) //item index nosunu aldik
        menuItem.setChecked(true)
    }
}
