package com.example.instagram_kotlinapp.Profile

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.instagram_kotlinapp.R
import com.example.instagram_kotlinapp.utils.BottomnavigationViewHelper
import kotlinx.android.synthetic.main.activity_profile.*


class ProfileActivity : AppCompatActivity() {

    private val ACTIVITY_NO=4 //bottom_navigation_menu.xml deki temlarin index nosudur.Ona gore hangi item oldugumusu anlariz
    private val TAG="Profile Activiy"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        setupNavigationView() //navigation vermedik
        setupToolbar()

    }

    private fun setupToolbar() {
        imgProfileSettings.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {

                var intent = Intent(this@ProfileActivity, ProfileSettingsActivity::class.java).addFlags(
                    Intent.FLAG_ACTIVITY_NO_ANIMATION)
                startActivity(intent)


            }


        })
    }

    fun setupNavigationView(){ //basta butonun yazisi olmasin basta kapattik
        BottomnavigationViewHelper.setupButtomNavigationView(bottomNavigationView)
        BottomnavigationViewHelper.setupNavigation(this,bottomNavigationView) //activty context gonderdik
        var menu=bottomNavigationView.menu //XML getirdik
        var menuItem=menu.getItem(ACTIVITY_NO) //item index nosunu aldik
        menuItem.setChecked(true)
    }
}
