package com.example.instagram_kotlinapp.News

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.instagram_kotlinapp.R
import com.example.instagram_kotlinapp.utils.BottomnavigationViewHelper
import kotlinx.android.synthetic.main.activity_main.*

class NewsActivity : AppCompatActivity() {

    private val ACTIVITY_NO=3 //bottom_navigation_menu.xml deki temlarin index nosudur.Ona gore hangi item oldugumusu anlariz
    private val TAG="News Activiy"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupNavigationView()

    }

    fun setupNavigationView(){ //basta butonun yazisi olmasin basta kapattik
        BottomnavigationViewHelper.setupButtomNavigationView(bottomNavigationView)
        BottomnavigationViewHelper.setupNavigation(this,bottomNavigationView) //activty context gonderdik
        var menu=bottomNavigationView.menu //XML getirdik
        var menuItem=menu.getItem(ACTIVITY_NO) //item index nosunu aldik
        menuItem.setChecked(true)
    }
}
