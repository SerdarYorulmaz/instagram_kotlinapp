package com.example.instagram_kotlinapp.Share

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.instagram_kotlinapp.R
import com.example.instagram_kotlinapp.utils.BottomnavigationViewHelper
import kotlinx.android.synthetic.main.activity_main.*

class ShareActivity : AppCompatActivity() {

    private val ACTIVITY_NO=2 //bottom_navigation_menu.xml deki temlarin index nosudur.Ona gore hangi item oldugumusu anlariz
    private val TAG="Share Activiy"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupNavigationView()

    }

    fun setupNavigationView(){ //basta butonun yazisi olmasin basta kapattik
        BottomnavigationViewHelper.setupButtomNavigationView(bottomNavigationView)
        BottomnavigationViewHelper.setupNavigation(this,bottomNavigationView) //activty context gonderdik
        var menu=bottomNavigationView.menu //XML getirdik (bu kodlar bu activity ile alaykalı oldugundan burda yaptık)
        var menuItem=menu.getItem(ACTIVITY_NO) //item index nosunu aldik
        menuItem.setChecked(true)
        }
}
