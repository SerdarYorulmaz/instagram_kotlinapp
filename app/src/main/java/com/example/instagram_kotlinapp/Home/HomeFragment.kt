package com.example.instagram_kotlinapp.Home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.instagram_kotlinapp.R

/*
Fragmentler de  activtylerde oldugu gibi create methodu var. Bu create methodu activity olsutudunda  layout icine olusuyor
savedInstanceState -->  cihazininz dik konumdan yatay konum alindiginda  normalde activty oldugunden layoutlar sil bastan olusturulur
veri saklmak icin  savedInstanceState kullnilir.

inflater --> xml dosyalriniz java koduna donusturen servis diye biliriz.(fragmenthome java kodu olarak gelmesi)
container --> xml  hangi layout yada view yerlestirilecegi yani attach yeri  gosterir.
 */

class HomeFragment :Fragment(){
    // fiel d alanı olacak sola kaydirdigimda kamera,sag da direct mesajları olacak

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var view=inflater?.inflate(R.layout.fragment_home,container,false) //rootdaki ozelliklerini sadece onun icin false,container hangi view eklenecek,hangi xml

        return  view
    }

}