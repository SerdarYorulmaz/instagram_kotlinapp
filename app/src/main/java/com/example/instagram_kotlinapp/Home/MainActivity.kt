package com.example.instagram_kotlinapp.Home

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.FragmentManager
import com.example.instagram_kotlinapp.R
import com.example.instagram_kotlinapp.utils.BottomnavigationViewHelper
import com.example.instagram_kotlinapp.utils.HomePageAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    /*
    MainACtivty  --> 3 fragment gecisini saglayan viewpage tutan yapı

        fragmenleride aktarma islemindede adapter kullanılır
        veri kaynaginda ver ialıp  sizin listesniz aktar ma islemi ypar -->adapter

        fragmentpageAdpter --> RECYLEVİEW  yaptıgımız gibi bu kez fragmentler icin yapacagiz
       fragment page adapter --> 3,5 tane fragment varsa onlar hafiza tutar ( bellekten silmez)
       fragment state page adater diye bir adpter var bu da bir cok fazla  fragment yapilarinda kullanilir.(bellekten siler gereksizleri)

     */

    private val ACTIVITY_NO=0 //bottom_navigation_menu.xml deki temlarin index nosudur.Ona gore hangi item oldugumusu anlariz
    private val TAG="Home Activiy"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupNavigationView()

        setupHomeViewPager()

    }


    fun setupNavigationView(){ //basta butonun yazisi olmasin basta kapattik
        BottomnavigationViewHelper.setupButtomNavigationView(bottomNavigationView)
        BottomnavigationViewHelper.setupNavigation(this,bottomNavigationView) //activty context gonderdik
        var menu=bottomNavigationView.menu //XML getirdik
        var menuItem=menu.getItem(ACTIVITY_NO) //item index nosunu aldik
        menuItem.setChecked(true)
    }

    private fun setupHomeViewPager() {

        //olusturudugum adapter siniftan instance uretilelim
        var homePageAdapter=HomePageAdapter(supportFragmentManager)
             //Gösterme sirasiyla
            homePageAdapter.addFragment(CameraFragment()) //id=0
        homePageAdapter.addFragment(HomeFragment()) //id=1
        homePageAdapter.addFragment(MessagesFragment()) //id=2

        homeViewPager.adapter=homePageAdapter //activty main  bulunan viewpager olusturdugumuz  adaptoru atadik
        homeViewPager.setCurrentItem(1) //viewpager homefragment ile baslasin
    }

}
