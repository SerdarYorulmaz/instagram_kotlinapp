package com.example.instagram_kotlinapp.utils

import android.content.Context
import android.content.Intent
import android.view.MenuItem
import com.example.instagram_kotlinapp.Home.MainActivity
import com.example.instagram_kotlinapp.News.NewsActivity
import com.example.instagram_kotlinapp.Profile.ProfileActivity
import com.example.instagram_kotlinapp.R
import com.example.instagram_kotlinapp.Search.SearchActivity
import com.example.instagram_kotlinapp.Share.ShareActivity
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.bottomnavigation.LabelVisibilityMode

//static bir sinif olusturarak tekrar cagiririz(var olan kodu tekrar tekrar kullan)
class BottomnavigationViewHelper {

    //static  sınıf
    companion object {

        fun setupButtomNavigationView(bottomNavigation: BottomNavigationView) {

            //butonlarimin baslangicta yazilari gozukmesin animasyolnarı kapalı olsun
            bottomNavigation.setLabelVisibilityMode(LabelVisibilityMode.LABEL_VISIBILITY_LABELED)
            bottomNavigation.clearAnimation()

        }

        //navigation islemleri burda olacak

        // oge tiklanidiginda yeni bir activity gidecegimden context nesnesine ihtiyacim var- BottomNavigationView birde tiklanmis ogrenmek icin bottom navigation ihtiyac var
        fun setupNavigation(context: Context, bottomNavigation: BottomNavigationView) {

            bottomNavigation.setOnNavigationItemSelectedListener(object :
                BottomNavigationView.OnNavigationItemSelectedListener {
                override fun onNavigationItemSelected(item: MenuItem): Boolean {

                    when (item.itemId) {

                        R.id.ic_home -> {

                            //activty icinde olmadimizdan context.startActivty gibi basina context koymaliyiz
                            //addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION) bize activty gecislerde default olan gelen animasyonu kapatmamizi sagladi
                            val intent = Intent(context, MainActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                            context.startActivity(intent)
                            return true
                        }


                        R.id.ic_news -> {


                            val intent = Intent(context, NewsActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                            context.startActivity(intent)
                            return true
                        }


                        R.id.ic_profile -> {


                            val intent = Intent(context, ProfileActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                            context.startActivity(intent)
                            return true
                        }


                        R.id.ic_search -> {


                            val intent = Intent(context, SearchActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                            context.startActivity(intent)
                            return true
                        }


                        R.id.ic_share -> {


                            val intent = Intent(context, ShareActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                            context.startActivity(intent)
                            return true
                        }

                    }

                    return false
                }
            })


        }
    }
}