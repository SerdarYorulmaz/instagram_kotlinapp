package com.example.instagram_kotlinapp.utils

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class HomePageAdapter(fm:FragmentManager) : FragmentPagerAdapter(fm) {

    /*
    FragmentManager tum fragmentleri yoneten siniftir

     */


    private var mFragmentList: ArrayList<Fragment> = ArrayList()

    //fragment pager methodlari
    override fun getItem(position: Int): Fragment {
        //ilgili listedeki fragmentladan ilgili ogeyi bulmak icin

      return   mFragmentList.get(position)

    }

    override fun getCount(): Int {
        // adapterdaki fragment listesinideki sayi gonderir

      return mFragmentList.size
    }


    fun addFragment(fragment: Fragment){
        mFragmentList.add(fragment)
    }

}